const express = require('express');
const fs=require('fs');
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true}));
const port = 3000;
const manejaGet = (req, res)=>{
    console.log("Estoy en la pagina home");
    res.json({saludo:'hola mundo desde node!'});
}
const readFile = (err, data)=>{
    if(err)console.log('Hubo un error');
    let info =JSON.parse(data);
    console.log(info);
    return info;
}
fs.readFile('Database/table.json', readFile);

app.get('/people', (req, res)=>{
   let data= fs.readFileSync('Database/table.json');
   let info =JSON.parse(data);
   console.log('Leimos el archivo', info);
   console.log(info);
   res.json({response: info})
})

app.get('/people/:id', (req, res)=>{
    //guardamos el id con que se realizó la petición
    let identifier = req.params.id;
    //leemos la información de la base de datos
    let data = fs.readFileSync('Database/table.json');
    let obJSON = JSON.parse(data);
    //tratamos de buscar el ientificador en la base de datos
    let response =null;
    for(let i =0; i<obJSON.length;i++){
        if(obJSON[i].id == identifier){
            response = obJSON[i];
            break;
        }
    }
   //Regreso lo que encontré
    if(response ==null){
    res.status(404).send();
    return ;
    }
    res.json(response);
})

app.post('/people', (req, res)=>{
    console.log(req.body);
    let person =req.body;
    //res.json(req.body);
    let data = fs.readFileSync('Database/table.json');
    let info = JSON.parse(data);
    for(let j =0; j<info.length;j++){
        if(info[j].id == person.id){
            res.status(400).json({mensaje: 'El usuario ya existe'});
            return ;
        }
    }
    info.push(person);
    fs.writeFileSync('Database/table.json', JSON.stringify(info));
    res.status(201).json(person);
})

app.get('/home', manejaGet);

app.get('/sergio', (req, res)=>{
   console.log('GET /Sergio');
   res.json({nombre:'Sergio Luis Xolo Absalón' }); 
});

app.listen(port, ()=>{
    console.log("El servidor esta escuchando en la url http://localhost:",port);

})